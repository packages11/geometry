    import XCTest
    import SceneKit
    @testable import Geometry

    final class GeometryTests: XCTestCase {
        #warning("need find planes angle")
        func test() {
            let pp1 = PlanePoints(point1: SCNVector3(-3, -1, -2), point2: SCNVector3(-3, 1, -2), point3: SCNVector3(3,1,-1))
            let pp2 = PlanePoints(point1: SCNVector3(-3,-1,1), point2: SCNVector3(-3,1,1), point3: SCNVector3(3,1,1))
            guard let ip = pp1.getIntersectionLineEquation(planePoints: pp2) else {
                return
            }
            print(ip)
        }
    }
