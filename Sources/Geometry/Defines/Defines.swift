public typealias Extremes<T> = (min: T, max: T)
