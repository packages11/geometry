public enum PointPositionOnDistance {
    case withinDistance
    case onSidePoint1
    case onSidePoint2
}
