import SceneKit

public struct RectangleCGPoints {
    public let bottomLeft: CGPoint
    public let topLeft: CGPoint
    public let bottomRight: CGPoint
    public let topRight: CGPoint

    public init(bottomLeft: CGPoint, topLeft: CGPoint, bottomRight: CGPoint, topRight: CGPoint) {
        self.bottomLeft = bottomLeft
        self.topLeft = topLeft
        self.bottomRight = bottomRight
        self.topRight = topRight
    }

}

public extension RectangleCGPoints {

    var size: CGSize {
        let width = CGFloat(bottomLeft.distance(to: bottomRight))
        let height = CGFloat(bottomLeft.distance(to: topLeft))
        return CGSize(width: width, height: height)
    }

    var maxSideLength: CGFloat {
        let size = size
        return max(size.width, size.height)
    }

    static func createRect(extremes: Extremes<CGPoint>) -> RectangleCGPoints {
        let bottomLeft = extremes.min
        let topLeft = CGPoint(x: extremes.min.x, y: extremes.max.y)
        let bottomRight = CGPoint(x: extremes.max.x, y: extremes.min.y)
        let topRight = extremes.max
        return RectangleCGPoints(bottomLeft: bottomLeft, topLeft: topLeft, bottomRight: bottomRight, topRight: topRight)
    }

    static func createRect(boundingBox: (min: SCNVector3, max: SCNVector3), twoDimensions: TwoDimensions) -> RectangleCGPoints {
        let (min, max) = boundingBox
        let minCg = min.getCgPoint(twoDimensions: twoDimensions)
        let maxCg = max.getCgPoint(twoDimensions: twoDimensions)
        let rectPoints = RectangleCGPoints.createRect(extremes: (minCg, maxCg))
        return rectPoints
    }
}
