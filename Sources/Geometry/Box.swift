import SceneKit

public struct Box<T> {
    public let width: T
    public let height: T
    public let length: T

    public init(width: T, height: T, length: T) {
        self.width = width
        self.height = height
        self.length = length
    }
}

public func *(box: Box<Float>, multiplier: SCNVector3) -> Box<Float> {
    Box(width: box.width * multiplier.x, height: box.height * multiplier.y, length: box.length * multiplier.z)
}
