import SceneKit

public struct LineEquation {
    public let point: SCNVector3
    public let vector: SCNVector3

    public init(point: SCNVector3, vector: SCNVector3) {
        self.point = point
        self.vector = vector
    }

    public func getPointOnLine(y: Float) -> SCNVector3 {
        let t = (y - point.y) / vector.y
        let x = point.x + t * vector.x
        let z = point.z + t * vector.z
        return SCNVector3(x, y, z)
    }
}
