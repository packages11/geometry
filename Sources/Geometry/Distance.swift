import SceneKit

public struct Distance {
    public let point1: SCNVector3
    public let point2: SCNVector3

    public var vector: SCNVector3 {
        point2 - point1
    }

    public var points: [SCNVector3] {
        [point1, point2]
    }

    public init(point1: SCNVector3, point2: SCNVector3) {
        self.point1 = point1
        self.point2 = point2
    }
}

public extension Distance {

    static var alongX: Distance {
        .init(point1: SCNVector3Zero, point2: SCNVector3(1, 0, 0))
    }
}

public extension Distance {

    func convert(from source: SCNNode, to target: SCNNode) -> Distance {
        let point1 = source.convertPosition(point1, to: target)
        let point2 = source.convertPosition(point2, to: target)
        let distance = Distance(point1: point1, point2: point2)
        return distance
    }
    
    func getProjectDistance(renderer: SCNSceneRenderer) -> Distance {
        let point1 = renderer.projectPoint(point1)
        let point2 = renderer.projectPoint(point2)
        return Distance(point1: point1, point2: point2)
    }
}

public extension Distance {

    /// Ccalculate normal distance between two skew lines.
    ///
    /// - Returns: Optional normal distance. If result is nil - lines is parallel.
    static func getNormalDistanceForSkewLines(line1: Distance, line2: Distance) -> Distance? {
        let a = line1.vector.dot(vector: line1.vector)
        let b = line1.vector.dot(vector: line2.vector)
        let e = line2.vector.dot(vector: line2.vector)
        let d = a * e - b * b
        guard d != 0 else {
            return nil
        }
        let r = line1.point1 - line2.point1
        let c = line1.vector.dot(vector: r)
        let f = line2.vector.dot(vector: r)
        let s = (b * f - c * e) / d
        let t = (a * f - c * b) / d
        let point1 = line1.point1 + line1.vector * s
        let point2 = line2.point1 + line2.vector * t
        let normalDistance = Distance(point1: point1, point2: point2)
        return normalDistance
    }
}

public extension Distance {

    var length: Float {
        point1.distance(to: point2)
    }

    var midPoint: SCNVector3 {
        point1.getMiddle(with: point2)
    }
    
    var swapped: Distance {
        Distance(point1: point2, point2: point1)
    }

    func getProjectPointOnLine(for point: SCNVector3) -> SCNVector3 {
        let vectorToPoint = point - point1
        let norm = vector.normalized()
        let dot = vectorToPoint.dot(vector: norm)
        let projectPointOnLine = point1 + norm * dot
        return projectPointOnLine
    }

    func getPointPosition(point: SCNVector3) -> PointPositionOnDistance {
        let vectorToPoint = point - point1
        let dot = vectorToPoint.dot(vector: vector)
        guard dot > 0 else {
            return .onSidePoint1
        }
        return vectorToPoint.length <= vector.length ? .withinDistance : .onSidePoint2
    }
    
    func getPoint(position: PointPositionOnDistance) -> SCNVector3? {
        switch position {
        case .withinDistance:
            return nil
        case .onSidePoint1:
            return point1
        case .onSidePoint2:
            return point2
        }
    }

    func getPointHorizontalSide(point: SCNVector3) -> HorizontalSide {
        let pointPosition = getPointPosition(point: point)
        switch pointPosition {
        case .withinDistance:
            let firstPartLength = point1.distance(to: point)
            let secondPartLength = point2.distance(to: point)
            return firstPartLength < secondPartLength ? .left : .right
        case .onSidePoint1:
            return .left
        case .onSidePoint2:
            return .right
        }
    }

    func makeZeroing(selection: SCNVector3.SelectOptions) -> Distance {
        Distance(point1: point1.makeZeroing(selection: selection), point2: point2.makeZeroing(selection: selection))
    }
}

extension Distance: Equatable {

    public static func == (lhs: Distance, rhs: Distance) -> Bool {
        return lhs.point1 == rhs.point1 &&
            lhs.point2 == rhs.point2
    }
}
