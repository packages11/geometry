import SceneKit

public class Cylinder: SCNNode {

    public init(color: UIColor = .white, radius: CGFloat = 0.025, height: CGFloat = 0.2) {
        super.init()
        name = Debug.NodeName.cylinder()
        geometry = SCNCylinder(radius: radius, height: height)
        geometry?.firstMaterial?.diffuse.contents = color
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    public override class var supportsSecureCoding: Bool {
        true
    }
}
