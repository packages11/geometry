public struct Debug {

    public enum NodeName {
        case sphere
        case origin
        case cylinder
        case line

        public func callAsFunction() -> String {
            "\(self)"+"-debug"
        }
    }
}
