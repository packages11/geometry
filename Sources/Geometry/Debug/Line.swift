import SceneKit

public final class Line: SCNNode {

    private var cylinderGeometry: SCNCylinder? {
        geometry as? SCNCylinder
    }

    public init(color: UIColor = .white, alpha: CGFloat = 0.6, radius: CGFloat = 0.0025) {
        super.init()
        let geometry = SCNCylinder(radius: radius, height: 0)
        let color = color.withAlphaComponent(alpha)
        geometry.firstMaterial = SCNMaterial.create(with: color)
        geometry.radialSegmentCount = 6
        self.geometry = geometry
        name = Debug.NodeName.line()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    public override class var supportsSecureCoding: Bool {
        true
    }
    
    public func update(with lineOrientation: LineOrientation) {
        transform = lineOrientation.transform
        cylinderGeometry?.height = lineOrientation.length
    }
}
