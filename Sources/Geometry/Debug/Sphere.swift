import SceneKit

public class Sphere: SCNNode {

    public init(color: UIColor = .white, radius: CGFloat = 0.025) {
        super.init()
        name = Debug.NodeName.sphere()
        geometry = SCNSphere(radius: radius)
        geometry?.firstMaterial?.diffuse.contents = color
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    public override class var supportsSecureCoding: Bool { true }
}
