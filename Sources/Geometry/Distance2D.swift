import SceneKit

public struct Distance2D {
    public let point1: CGPoint
    public let point2: CGPoint
    
    public var vector: CGPoint {
        point2 - point1
    }
    
    public var midPoint: CGPoint {
        point1.getMiddle(with: point2)
    }
    
    public var points: [CGPoint] {
        [point1, point2]
    }

    public init(point1: CGPoint, point2: CGPoint) {
        self.point1 = point1
        self.point2 = point2
    }
}

public extension Distance2D {
    static func create(from distance: Distance, twoDimensions: TwoDimensions) -> Distance2D {
        Distance2D(point1: distance.point1.getCgPoint(twoDimensions: twoDimensions), 
                   point2: distance.point2.getCgPoint(twoDimensions: twoDimensions))
    }
    
    func getIntersection(with distance: Distance2D) -> CGPoint? {
        let xDiff1 = point1.x - point2.x
        let xDiff2 = distance.point1.x - distance.point2.x
        let yDiff1 = point1.y - point2.y
        let yDiff2 = distance.point1.y - distance.point2.y
        let denominator = xDiff1 * yDiff2 - yDiff1 * xDiff2
        guard denominator != 0 else {
            return nil
        }
        let multDiff1 = point1.x * point2.y - point1.y * point2.x
        let multDiff2 = distance.point1.x * distance.point2.y - distance.point1.y * distance.point2.x
        let x = (multDiff1 * xDiff2 - xDiff1 * multDiff2) / denominator
        let y = (multDiff1 * yDiff2 - yDiff1 * multDiff2) / denominator
        return CGPoint(x: x, y: y)
    }
    
    var length: CGFloat {
        CGFloat(point1.distance(to: point2))
    }
    
    var swapped: Distance2D {
        Distance2D(point1: point2, point2: point1)
    }
    
    func containsInBounds(point: CGPoint) -> Bool {
        let boundingRect = points.getBoundingRect()
        return point.x >= boundingRect.minX &&
               point.x <= boundingRect.maxX &&
               point.y >= boundingRect.minY &&
               point.y <= boundingRect.maxY
    }
    
    func getPointPosition(point: CGPoint) -> PointPositionOnDistance {
        let vectorToPoint = point - point1
        let dot = vectorToPoint.dot(point: vector)
        if dot < 0 {
            return .onSidePoint1
        } else if dot == 0 {
            return .withinDistance
        } else {
            return vectorToPoint.length <= vector.length ? .withinDistance : .onSidePoint2
        }
    }
    
    func orderFromLeftToRight(pointOfView: CGPoint) -> Distance2D {
        var p1 = SCNVector3(x: Float(point1.x), y: 0, z: Float(point1.y))
        var p2 = SCNVector3(x: Float(point2.x), y: 0, z: Float(point2.y))
        let pointOfView = SCNVector3(x: Float(pointOfView.x), y: 0, z: Float(pointOfView.y))
        SCNVector3.orderFromLeftToRight(left: &p1, right: &p2, pointOfView: pointOfView)
        let point1 = p1.getCgPoint(twoDimensions: .xz)
        let point2 = p2.getCgPoint(twoDimensions: .xz)
        return Distance2D(point1: point1, point2: point2)
    }
    
    func getAngleInRadians(with distance: Distance2D) -> CGFloat {
        vector.getAngleInRadians(with: distance.vector)
    }
    
    func getAngleInDegrees(with distance: Distance2D) -> CGFloat {
        vector.getAngleInDegrees(with: distance.vector)
    }
    
    func getProjectPointOnLine(for point: CGPoint) -> CGPoint {
        let vectorToPoint = point - point1
        let norm = vector.normalized()
        let dot = vectorToPoint.dot(point: norm)
        let projectPoint = point1 + norm * dot
        return projectPoint
    }
}

extension Distance2D: Equatable {
    public static func == (lhs: Distance2D, rhs: Distance2D) -> Bool {
        return lhs.point1 == rhs.point1 &&
            lhs.point2 == rhs.point2
    }
}
