import SceneKit

public extension SCNMatrix4 {
    
    init(x: SCNVector3, y: SCNVector3, z: SCNVector3, w: SCNVector3) {
        self.init(
            m11: x.x,
            m12: x.y,
            m13: x.z,
            m14: 0.0,
            
            m21: y.x,
            m22: y.y,
            m23: y.z,
            m24: 0.0,
            
            m31: z.x,
            m32: z.y,
            m33: z.z,
            m34: 0.0,
            
            m41: w.x,
            m42: w.y,
            m43: w.z,
            m44: 1.0)
    }
    
    static func getTransform(startPoint: SCNVector3, endPoint: SCNVector3) -> SCNMatrix4 {
        let lookAt = endPoint - startPoint
        let y = lookAt.normalized()
        let up = lookAt.cross(vector: endPoint).normalized()
        let x = y.cross(vector: up).normalized()
        let z = x.cross(vector: y).normalized()
        let transform = SCNMatrix4(x: x, y: y, z: z, w: startPoint)
        return transform
    }
}

public func * (left: SCNMatrix4, right: SCNMatrix4) -> SCNMatrix4 {
    return SCNMatrix4Mult(left, right)
}
