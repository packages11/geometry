import SceneKit

public extension SCNVector3 {
    
    var length: Float {
        sqrtf(x*x + y*y + z*z)
    }
    
    func normalized() -> SCNVector3 {
        let length = length
        return length.isZero ? SCNVector3Zero : self / length
    }
    
    func dot(vector: SCNVector3) -> Float {
        x * vector.x + y * vector.y + z * vector.z
    }
    
    func cross(vector: SCNVector3) -> SCNVector3 {
        SCNVector3Make(y * vector.z - z * vector.y, z * vector.x - x * vector.z, x * vector.y - y * vector.x)
    }
    
    func distance(to destination: SCNVector3) -> Float {
        let diff = destination - self
        let distance = diff.length
        return distance
    }
    
    func getCgPoint(twoDimensions: TwoDimensions) -> CGPoint {
        switch twoDimensions {
        case .xy:
            return CGPoint(x: CGFloat(x), y: CGFloat(y))
        case .yz:
            return CGPoint(x: CGFloat(y), y: CGFloat(z))
        case .xz:
            return CGPoint(x: CGFloat(x), y: CGFloat(z))
        }
    }
    
    var simd: simd_float3 {
        SIMD3(self)
    }
    
    func isNearByY(with vector: SCNVector3, limit: Float) -> Bool {
        let difference = abs(y - vector.y)
        return difference <= limit
    }

    func getMiddle(with point: SCNVector3) -> SCNVector3 {
        let x = (self.x + point.x) / 2
        let y = (self.y + point.y) / 2
        let z = (self.z + point.z) / 2
        return SCNVector3(x, y, z)
    }

    func getOppositeCornerPoint(diagonal: Distance) -> SCNVector3 {
        let midPoint = diagonal.midPoint
        let distanceToMidPoint = midPoint.distance(to: self)
        let distance = distanceToMidPoint * 2
        let normVector = (midPoint - self).normalized()
        let mirrorPoint = self + (normVector * distance)
        return mirrorPoint
    }

    func getAngleInRadians(with vector: SCNVector3) -> Float {
        let dot = dot(vector: vector)
        var angle = dot / (length * vector.length)
        let one: Float = angle < 0 ? -1 : 1
        angle = abs(angle) > 1 ? one : angle
        return acos(angle)
    }

    func getAngleInDegrees(with vector: SCNVector3) -> Float {
        getAngleInRadians(with: vector).radiansToDegrees
    }

    func getProjectPointToPlane(_ plane: SCNNode, root: SCNNode, zeroing: SCNVector3.SelectOptions) -> SCNVector3 {
        var projectPoint = root.convertPosition(self, to: plane)
        projectPoint = projectPoint.makeZeroing(selection: zeroing)
        projectPoint = plane.convertPosition(projectPoint, to: root)
        return projectPoint
    }

    func makeZeroing(selection: SCNVector3.SelectOptions) -> SCNVector3 {
        var vector = self
        if selection.contains(.x) {
            vector.x = 0
        }
        if selection.contains(.y) {
            vector.y = 0
        }
        if selection.contains(.z) {
            vector.z = 0
        }
        return vector
    }

    func isEqual(to vector: SCNVector3, tolerance: Float) -> Bool {
        if abs(x - vector.x) > tolerance {
            return false
        }
        if abs(y - vector.y) > tolerance {
            return false
        }
        if abs(z - vector.z) > tolerance {
            return false
        }
        return true
    }
}

extension SCNVector3: @retroactive Equatable {
    
    public static func == (lhs: SCNVector3, rhs: SCNVector3) -> Bool {
        return lhs.x == rhs.x &&
            lhs.y == rhs.y &&
            lhs.z == rhs.z
    }
}

extension SCNVector3: @retroactive Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(x)
        hasher.combine(y)
        hasher.combine(z)
    }
}

public extension SCNVector3 {
    
    static func orderAscending(point1: inout SCNVector3, point2: inout SCNVector3, options: SelectOptions) {
        if options.contains(.x),
           point2.x < point1.x {
            return swap(&point1, &point2)
        }
        if options.contains(.y),
           point2.y < point1.y {
            return swap(&point1, &point2)
        }
        if options.contains(.z),
           point2.z < point1.z {
            return swap(&point1, &point2)
        }
    }

    static func orderFromLeftToRight(left: inout SCNVector3, right: inout SCNVector3, pointOfView: SCNVector3) {
        let diff1 = left - pointOfView
        let diff2 = right - left
        let cross = diff1.cross(vector: diff2)
        if cross.y > 0 {
            swap(&left, &right)
        }
    }
}

public extension SCNVector3 {

    struct SelectOptions: OptionSet {
        public let rawValue: Int

        public init(rawValue: Int) {
            self.rawValue = rawValue
        }

        public static let x = SelectOptions(rawValue: 1 << 0)
        public static let y = SelectOptions(rawValue: 1 << 1)
        public static let z = SelectOptions(rawValue: 1 << 2)
        public static let all: SelectOptions = [.x, .y, .z]
    }
}

public extension SCNVector3 {

    init(repeated: Float) {
        self.init(repeated, repeated, repeated)
    }
}

public extension SCNVector3 {
    static let right = SCNVector3(x: 1, y: 0, z: 0)
    static let up = SCNVector3(x: 0, y: 1, z: 0)
    static let forward = SCNVector3(x: 0, y: 0, z: 1)
    static let backward = SCNVector3(x: 0, y: 0, z: -1)
}

public extension SCNVector3 {
    var shortDescription: String {
        "SCNVector3(\(x), \(y), \(z))"
    }
}

public func +(left: SCNVector3, right: SCNVector3) -> SCNVector3 {
    SCNVector3(left.x + right.x, left.y + right.y, left.z + right.z)
}

public func -(left: SCNVector3, right: SCNVector3) -> SCNVector3 {
    left + (right * -1.0)
}

public func *(vector: SCNVector3, multiplier: SCNFloat) -> SCNVector3 {
    SCNVector3(vector.x * multiplier, vector.y * multiplier, vector.z * multiplier)
}

public func / (vector: SCNVector3, scalar: Float) -> SCNVector3 {
    SCNVector3Make(vector.x / scalar, vector.y / scalar, vector.z / scalar)
}
