import SceneKit

public extension SCNGeometry {

    static func polygonPlane(vertices: [SCNVector3]) -> SCNGeometry {
        let verticesCount = Int32(vertices.count)
        var indices: [Int32] = [verticesCount]
        indices += Array(0..<verticesCount)
        let vertexSource = SCNGeometrySource(vertices: vertices)
        let intTypeSize = MemoryLayout<Int32>.size
        let indexData = Data(bytes: indices, count: indices.count * intTypeSize)
        let element = SCNGeometryElement(data: indexData, primitiveType: .polygon, primitiveCount: 1, bytesPerIndex: intTypeSize)
        let geometry = SCNGeometry(sources: [vertexSource], elements: [element])
        return geometry
    }

    func addTextureSourceForRectanglePolygon() -> SCNGeometry {
        let x: CGFloat
        let y: CGFloat
        if let vertices = vertices,
           vertices.count == 4 {
            let width = vertices[0].distance(to: vertices[3])
            let height = vertices[0].distance(to: vertices[1])
            x = width < 1 ? CGFloat(width) : 1
            y = height < 1 ? CGFloat(height) : 1
        } else {
            x = 1
            y = 1
        }
        let textureCoordinates = [CGPoint(x: 0, y: 0), CGPoint(x: 0, y: y), CGPoint(x: x, y: y), CGPoint(x: x, y: 0)]
        return getGeometry(textureCoordinates: textureCoordinates)
    }

    func addTextureSourceForPolygon(twoDimensions: TwoDimensions) -> SCNGeometry {
        let rectPoints = RectangleCGPoints.createRect(boundingBox: boundingBox, twoDimensions: twoDimensions)
        let vertices = vertices ?? []
        let cgVertices = vertices.map({ $0.getCgPoint(twoDimensions: twoDimensions) })
        return addTextureSourceForPolygon(rectPoints: rectPoints, cgVertices: cgVertices)
    }

    func addTextureSourceForPolygon(rectPoints: RectangleCGPoints, cgVertices: [CGPoint]) -> SCNGeometry {
        let maxSideLength = rectPoints.maxSideLength
        let multiplier = maxSideLength > 1 ? 1 / maxSideLength : 1
        let origin = rectPoints.bottomLeft
        let textureCoordinates = cgVertices.map { vertex -> CGPoint in
            let shiftedVertex = vertex - origin
            let converted = shiftedVertex * multiplier
            return converted
        }
        return getGeometry(textureCoordinates: textureCoordinates)
    }

    private func getGeometry(textureCoordinates: [CGPoint]) -> SCNGeometry {
        let textureSource = SCNGeometrySource(textureCoordinates: textureCoordinates)
        let sources = [sources(for: .vertex).first, textureSource].compactMap({ $0 })
        let geometry = SCNGeometry(sources: sources, elements: elements)
        return geometry
    }

    func apply(imageName: String, scale: CGSize) {
        let transformScale = SCNMatrix4MakeScale(Float(scale.width), Float(scale.height), 0)
        firstMaterial?.diffuse.contents = imageName
        firstMaterial?.diffuse.contentsTransform = transformScale
    }

    var vertices: [SCNVector3]? {
        guard let source  = sources(for: .vertex).first else{
            return nil
        }
        return source.data.withUnsafeBytes { buffer in
            [SCNVector3](buffer.bindMemory(to: SCNVector3.self))
        }
    }
}
