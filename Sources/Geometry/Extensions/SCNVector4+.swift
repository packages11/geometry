import SceneKit

public extension SCNVector4 {
    var сomponents: [Float] {
        [x, y, z, w]
    }
}

public func +(left: SCNVector4, right: SCNVector4) -> SCNVector4 {
    SCNVector4(left.x + right.x, left.y + right.y, left.z + right.z, left.w + right.w)
}

public func / (vector: SCNVector4, scalar: Float) -> SCNVector4 {
    SCNVector4(vector.x / scalar, vector.y / scalar, vector.z / scalar, vector.w / scalar)
}
