import UIKit

public extension CGSize {
    var max: CGFloat {
        Swift.max(width, height)
    }

    var min: CGFloat {
        Swift.min(width, height)
    }

    var area: CGFloat {
        width * height
    }
}

public func *(size: CGSize, multiplier: CGFloat) -> CGSize {
    CGSize(width: size.width * multiplier, height: size.height * multiplier)
}
