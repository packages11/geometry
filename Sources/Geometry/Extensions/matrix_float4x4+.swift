import SceneKit

public extension matrix_float4x4 {
    var vector: SCNVector3 {
        SCNVector3(columns.3.x, columns.3.y, columns.3.z)
    }

    var eulerAngles: SCNVector3 {
        SCNVector3(
            x: asin(-self[2][1]),
            y: atan2(self[2][0], self[2][2]),
            z: atan2(self[0][1], self[1][1])
        )
    }
}
