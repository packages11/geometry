import ARKit

public extension ARAnchor {

    func isEqual(anchor: ARAnchor) -> Bool {
        identifier == anchor.identifier
    }
}
