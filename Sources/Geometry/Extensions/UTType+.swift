import Foundation
import UniformTypeIdentifiers

public extension UTType {
    init?(fileName: String) {
        let nameParts = fileName.components(separatedBy: ".")
        guard nameParts.count > 1,
                let filenameExtension = nameParts.last else {
            return nil
        }
        self.init(filenameExtension: filenameExtension)
    }

    var isImage: Bool {
        conforms(to: UTType.image)
    }
}
