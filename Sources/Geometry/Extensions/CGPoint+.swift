import UIKit
import SceneKit

public extension Array where Element == CGPoint {

    func calculateArea() -> CGFloat {
        var sum: CGFloat = 0
        for index in 0..<count {
            let nextIndex = (index + 1 < count) ? index + 1 : 0
            let point = self[index]
            let nextPoint = self[nextIndex]
            sum += point.x * nextPoint.y - point.y * nextPoint.x
        }
        let area = abs(sum) / 2
        return area
    }
    
    func getBoundingRect() -> CGRect {
        guard let firstPoint = first else {
            return .zero
        }
        var minX: CGFloat = firstPoint.x
        var maxX: CGFloat = firstPoint.x
        var minY: CGFloat = firstPoint.y
        var maxY: CGFloat = firstPoint.y
        let points = dropFirst()
        for point in points {
            if minX > point.x {
                minX = point.x
            }
            if maxX < point.x {
                maxX = point.x
            }
            if minY > point.y {
                minY = point.y
            }
            if maxY < point.y {
                maxY = point.y
            }
        }
        let origin = CGPoint(x: minX, y: minY)
        let size = CGSize(width: maxX - minX, height: maxY - minY)
        return CGRect(origin: origin, size: size)
    }
}

public extension CGPoint {
    var length: Float {
        sqrtf(Float(x*x + y*y))
    }
    
    var point3D: SCNVector3 {
        SCNVector3(x: Float(x), y: Float(y), z: 0)
    }

    func normalized() -> CGPoint {
        self / CGFloat(length)
    }

    func distance(to destination: CGPoint) -> Float {
        let diff = destination - self
        let distance = diff.length
        return distance
    }
    
    func getMiddle(with point: CGPoint) -> CGPoint {
        let x = (x + point.x) / 2
        let y = (y + point.y) / 2
        return CGPoint(x: x, y: y)
    }

    func dot(point: CGPoint) -> CGFloat {
        x * point.x + y * point.y
    }
    
    func getAngleInRadians(with vector: CGPoint) -> CGFloat {
        let dot = dot(point: vector)
        let lengthMult = CGFloat(length * vector.length)
        guard lengthMult > 0 else {
            return 0
        }
        let value = dot / lengthMult
        let angle = acos(value)
        guard !angle.isNaN else {
            return 0
        }
        let sign: CGFloat = isPositiveAngle(with: vector) ? 1 : -1
        return sign * angle
    }
    
    func getAngleInDegrees(with vector: CGPoint) -> CGFloat {
        getAngleInRadians(with: vector).radiansToDegrees
    }
    
    func isPositiveAngle(with vector: CGPoint) -> Bool {
        let vector1 = SCNVector3(x: Float(x), y: 0, z: Float(y))
        let vector2 = SCNVector3(x: Float(vector.x), y: 0, z: Float(vector.y))
        let cross = vector1.cross(vector: vector2)
        return cross.y >= 0
    }
    
    func rotated(degrees: CGFloat) -> CGPoint {
        rotated(radians: degrees.degreesToRadians)
    }
    
    func rotated(radians: CGFloat) -> CGPoint {
        let radians = -radians
        let x = self.x * cos(radians) - self.y * sin(radians)
        let y = self.x * sin(radians) + self.y * cos(radians)
        return CGPoint(x: x, y: y)
    }
    
    func with(dX: CGFloat?, dY: CGFloat?) -> CGPoint {
        var x = x
        var y = y
        if let dX = dX {
            x += dX
        }
        if let dY = dY {
            y += dY
        }
        return CGPoint(x: x, y: y)
    }
}

public extension CGPoint {
    static let right = CGPoint(x: 1, y: 0)
    static let left = CGPoint(x: -1, y: 0)
    static let up = CGPoint(x: 0, y: -1)
    static let down = CGPoint(x: 0, y: 1)
}

public func +(left: CGPoint, right: CGPoint) -> CGPoint {
    CGPoint(x: left.x + right.x, y: left.y + right.y)
}

public func -(left: CGPoint, right: CGPoint) -> CGPoint {
    left + (right * -1.0)
}

public func *(vector: CGPoint, multiplier: CGFloat) -> CGPoint {
    CGPoint(x: vector.x * multiplier, y: vector.y * multiplier)
}

public func / (vector: CGPoint, scalar: CGFloat) -> CGPoint {
    CGPoint(x: vector.x / scalar, y: vector.y / scalar)
}
