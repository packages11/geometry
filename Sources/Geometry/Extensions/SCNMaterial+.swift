import SceneKit
import AVFoundation
import UniformTypeIdentifiers

public extension SCNMaterial {

    static func create(with color: UIColor) -> SCNMaterial {
        let material = SCNMaterial()
        material.diffuse.contents = color
        material.isDoubleSided = true
        return material
    }

    static var transparented: SCNMaterial {
        let material = SCNMaterial()
        material.colorBufferWriteMask = []
        material.isDoubleSided = true
        return material
    }

    var hasImage: Bool {
        if let contentsData = diffuse.contents as? Data {
            let hasTexture = UIImage(data: contentsData) != nil
            return hasTexture
        }
        if diffuse.contents is UIImage {
            return true
        }
        if let imagePath = diffuse.contents as? String,
           let utType = UTType(fileName: imagePath) {
            return utType.isImage
        }
        return false
    }

    var hasAVPlayer: Bool {
        diffuse.contents is AVPlayer
    }
}
