import SceneKit

public extension SCNPlane {

    var size: CGSize {
        get {
            CGSize(width: width, height: height)
        }
        set {
            width = newValue.width
            height = newValue.height
        }
    }
}
