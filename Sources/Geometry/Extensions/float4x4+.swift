import SceneKit

public extension float4x4 {
    
    var translation: SIMD3<Float> {
        get {
            let translation = columns.3
            return [translation.x, translation.y, translation.z]
        }
        set(newValue) {
            columns.3 = [newValue.x, newValue.y, newValue.z, columns.3.w]
        }
    }
    
    var position: SCNVector3 {
        SCNVector3(translation)
    }
    
    var orientation: simd_quatf {
        simd_quaternion(self)
    }
    
    var rotation: SCNVector4 {
        let orientation = orientation
        return SCNVector4(orientation.axis.x, orientation.axis.y, orientation.axis.z, orientation.angle)
    }

    init(translation vector: SCNVector3) {
        self.init(SIMD4(1, 0, 0, 0),
                  SIMD4(0, 1, 0, 0),
                  SIMD4(0, 0, 1, 0),
                  SIMD4(vector.x, vector.y, vector.z, 1))
    }
}
