import SceneKit

public extension SCNNode {

    var root: SCNNode? {
        let root = getRoot(for: self)
        return root !== self ? root : nil
    }

    private func getRoot(for node: SCNNode) -> SCNNode {
        if let node = node.parent {
            return getRoot(for: node)
        } else {
            return node
        }
    }

    var hasTexture: Bool {
        geometry?.firstMaterial?.hasImage == true
    }
    
    var boundingBoxSizes: Box<Float> {
        let (min, max) = boundingBox
        return Box(width: max.x - min.x,
                   height: max.y - min.y,
                   length: max.z - min.z)
    }
    
    var worldBoundingBox: (min: SCNVector3, max: SCNVector3) {
        let (min, max) = boundingBox
        let worldMin = convertPosition(min, to: nil)
        let worldMax = convertPosition(max, to: nil)
        return (worldMin, worldMax)
    }

    convenience init(name: String) {
        self.init()
        self.name = name
    }

    convenience init(name: String, geometry: SCNGeometry?) {
        self.init(geometry: geometry)
        self.name = name
    }
    
    func belongsToSubTree(of node: SCNNode) -> Bool {
        if self === node {
            return true
        }
        if let parent = parent {
            return parent.belongsToSubTree(of: node)
        } else {
            return false
        }
    }
    
}
