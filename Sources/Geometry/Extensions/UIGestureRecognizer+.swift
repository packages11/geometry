import SceneKit

public extension UIGestureRecognizer {

    func getHitTestResult(view: SCNView) -> SCNHitTestResult? {
        let locationInView = location(in: view)
        let results = view.hitTest(locationInView, options: nil)
        let testResult = results.first
        return testResult
    }
}
