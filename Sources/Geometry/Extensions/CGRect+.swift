import UIKit

public extension CGRect {
    var topLeft: CGPoint {
        CGPoint(x: minX, y: minY)
    }
    
    var topRight: CGPoint {
        CGPoint(x: maxX, y: minY)
    }
    
    var bottomRight: CGPoint {
        CGPoint(x: maxX, y: maxY)
    }
    
    var bottomLeft: CGPoint {
        CGPoint(x: minX, y: maxY)
    }
    
    var center: CGPoint {
        CGPoint(x: midX, y: midY)
    }
    
    var left: Distance2D {
        Distance2D(point1: bottomLeft, point2: topLeft)
    }
    
    var right: Distance2D {
        Distance2D(point1: topRight, point2: bottomRight)
    }
    
    var top: Distance2D {
        Distance2D(point1: topLeft, point2: topRight)
    }
    
    var bottom: Distance2D {
        Distance2D(point1: bottomRight, point2: bottomLeft)
    }
    
    var edges: [Distance2D] {
        [left, top, right, bottom]
    }
    
    func contains(distance: Distance2D) -> Bool {
        distance.points.filter({ contains($0) }).count == distance.points.count
    }
}
