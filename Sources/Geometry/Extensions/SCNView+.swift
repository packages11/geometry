import SceneKit

public extension SCNView {

    func getHitTestResults(point: CGPoint? = nil) -> [SCNHitTestResult] {
        let point = point ?? center
        let hitTestResults = hitTest(point, options: [.searchMode: SCNHitTestSearchMode.all.rawValue as NSNumber])
        return hitTestResults
    }
}
