import SceneKit

public struct PlaneEquation {
    public let a: Float
    public let b: Float
    public let c: Float
    public let d: Float

    public var normal: SCNVector3 {
        SCNVector3(a, b, c)
    }

    public init(a: Float, b: Float, c: Float, d: Float) {
        self.a = a
        self.b = b
        self.c = c
        self.d = d
    }

    public static func create(planePoints: PlanePoints) -> PlaneEquation {
        let a1 = planePoints.point2.x - planePoints.point1.x
        let b1 = planePoints.point2.y - planePoints.point1.y
        let c1 = planePoints.point2.z - planePoints.point1.z
        let a2 = planePoints.point3.x - planePoints.point1.x
        let b2 = planePoints.point3.y - planePoints.point1.y
        let c2 = planePoints.point3.z - planePoints.point1.z
        let a = b1 * c2 - b2 * c1
        let b = a2 * c1 - a1 * c2
        let c = a1 * b2 - b1 * a2
        let d = a * planePoints.point1.x + b * planePoints.point1.y + c * planePoints.point1.z
        let equation = PlaneEquation(a: a, b: b, c: c, d: d)
        return equation
    }
}

public extension PlaneEquation {
    func getIntersection(with distance: Distance) -> SCNVector3? {
        let lineVector = distance.vector
        let dot1 = normal.dot(vector: distance.point1)
        let dot2 = normal.dot(vector: lineVector)
        guard !dot2.isZero else {
            return nil
        }
        let point = distance.point1 + lineVector * ((d - dot1) / dot2)
        return point
    }
}
