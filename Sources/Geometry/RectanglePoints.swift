import SceneKit

public struct RectanglePoints {
    public let bottomLeft: SCNVector3
    public let topLeft: SCNVector3
    public let bottomRight: SCNVector3
    public let topRight: SCNVector3

    public init(bottomLeft: SCNVector3, topLeft: SCNVector3, bottomRight: SCNVector3, topRight: SCNVector3) {
        self.bottomLeft = bottomLeft
        self.topLeft = topLeft
        self.bottomRight = bottomRight
        self.topRight = topRight
    }
}

public extension RectanglePoints {

    static func create(sideExtremes: Extremes<SCNVector3>, yExtermes: Extremes<Float>) -> RectanglePoints {
        var bottomLeft = sideExtremes.min
        var bottomRight = sideExtremes.max
        var topLeft = sideExtremes.min
        var topRight = sideExtremes.max
        bottomLeft.y = yExtermes.min
        bottomRight.y = yExtermes.min
        topLeft.y = yExtermes.max
        topRight.y = yExtermes.max
        return RectanglePoints(bottomLeft: bottomLeft, topLeft: topLeft, bottomRight: bottomRight, topRight: topRight)
    }

    static func createXyRect(diagonalPoint1: SCNVector3, diagonalPoint2: SCNVector3) -> RectanglePoints {
        var leftPoint = diagonalPoint1
        var rightPoint = diagonalPoint2
        SCNVector3.orderAscending(point1: &leftPoint, point2: &rightPoint, options: .x)
        let bottomLeft: SCNVector3
        let topLeft: SCNVector3
        let bottomRight: SCNVector3
        let topRight: SCNVector3
        if leftPoint.y < rightPoint.y {
            bottomLeft = leftPoint
            topLeft = SCNVector3(leftPoint.x, rightPoint.y, 0)
            bottomRight = SCNVector3(rightPoint.x, leftPoint.y, 0)
            topRight = rightPoint
        } else {
            bottomLeft = SCNVector3(leftPoint.x, rightPoint.y, 0)
            topLeft = leftPoint
            bottomRight = rightPoint
            topRight = SCNVector3(rightPoint.x, leftPoint.y, 0)
        }
        return RectanglePoints(bottomLeft: bottomLeft, topLeft: topLeft, bottomRight: bottomRight, topRight: topRight)
    }

    static func createVertical(with boundingBox: (min: SCNVector3, max: SCNVector3)) -> RectanglePoints {
        createVerticalRectangle(min: boundingBox.min, max: boundingBox.max)
    }

    static func createVerticalRectangle(min: SCNVector3, max: SCNVector3) -> RectanglePoints {
        let bottomLeft = min
        let topLeft = SCNVector3(min.x, max.y, min.z)
        let bottomRight = SCNVector3(max.x, min.y, max.z)
        let topRight = max
        return RectanglePoints(bottomLeft: bottomLeft, topLeft: topLeft, bottomRight: bottomRight, topRight: topRight)
    }

    static func createHorizontal(with boundingBox: (min: SCNVector3, max: SCNVector3)) -> RectanglePoints {
        createHorizontalRectangle(min: boundingBox.min, max: boundingBox.max)
    }

    static func createHorizontalRectangle(min: SCNVector3, max: SCNVector3) -> RectanglePoints {
        let topLeft = min
        let bottomLeft = SCNVector3(min.x, min.y, max.z)
        let topRight = SCNVector3(max.x, min.y, min.z)
        let bottomRight = max
        return RectanglePoints(bottomLeft: bottomLeft, topLeft: topLeft, bottomRight: bottomRight, topRight: topRight)
    }

    var vertices: [SCNVector3] {
        [bottomLeft, topLeft, topRight, bottomRight]
    }

    var eulerAngleY: Float {
        let distance = Distance(point1: bottomLeft, point2: bottomRight)
        let norm1 = Distance.alongX.vector.normalized()
        let norm2 = distance.vector.normalized()
        let sign: Float = norm2.z < 0 ? 1 : -1
        let dot = norm1.dot(vector: norm2)
        return sign * acos(dot)
    }

    var size: CGSize {
        let width = CGFloat(bottomLeft.distance(to: bottomRight))
        let height = CGFloat(bottomLeft.distance(to: topLeft))
        return CGSize(width: width, height: height)
    }

    var bottomDistance: Distance {
        Distance(point1: bottomLeft, point2: bottomRight)
    }

    var topDistance: Distance {
        Distance(point1: topLeft, point2: topRight)
    }

    var leftDistance: Distance {
        Distance(point1: bottomLeft, point2: topLeft)
    }

    var rightDistance: Distance {
        Distance(point1: bottomRight, point2: topRight)
    }

    var planePoints: PlanePoints {
        PlanePoints(point1: bottomLeft, point2: bottomRight, point3: topLeft)
    }

    func convert(from source: SCNNode, to target: SCNNode) -> RectanglePoints {
        let bottomLeft = source.convertPosition(self.bottomLeft, to: target)
        let topLeft = source.convertPosition(self.topLeft, to: target)
        let topRight = source.convertPosition(self.topRight, to: target)
        let bottomRight = source.convertPosition(self.bottomRight, to: target)
        return RectanglePoints(bottomLeft: bottomLeft, topLeft: topLeft, bottomRight: bottomRight, topRight: topRight)
    }
}
