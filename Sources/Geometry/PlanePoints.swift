import SceneKit

public struct PlanePoints {
    public let point1: SCNVector3
    public let point2: SCNVector3
    public let point3: SCNVector3

    public init(point1: SCNVector3, point2: SCNVector3, point3: SCNVector3) {
        self.point1 = point1
        self.point2 = point2
        self.point3 = point3
    }
}

public extension PlanePoints {

    static var horizontalPlane: PlanePoints {
        .init(point1: SCNVector3Zero, point2: SCNVector3(1, 0, 0), point3: SCNVector3(0, 0, 1))
    }

    static var verticalPlane: PlanePoints {
        .init(point1: SCNVector3Zero, point2: SCNVector3(1, 0, 0), point3: SCNVector3(0, 1, 0))
    }
}

public extension PlanePoints {

    func convert(from source: SCNNode, to target: SCNNode) -> PlanePoints {
        let point1 = source.convertPosition(self.point1, to: target)
        let point2 = source.convertPosition(self.point2, to: target)
        let point3 = source.convertPosition(self.point3, to: target)
        let planePoints = PlanePoints(point1: point1, point2: point2, point3: point3)
        return planePoints
    }
}

public extension PlanePoints {

    var normal: SCNVector3 {
        let vector1 = Distance(point1: point1, point2: point2).vector
        let vector2 = Distance(point1: point1, point2: point3).vector
        let normal = vector1.cross(vector: vector2).normalized()
        return normal
    }

    func getIntersectionLineEquation(planePoints: PlanePoints) -> LineEquation? {
        let lineVector = normal.cross(vector: planePoints.normal)
        let lineDirection = planePoints.normal.cross(vector: lineVector)
        let denominator = normal.dot(vector: lineDirection)
        guard abs(denominator) > 0 else {
            return nil
        }
        let diffPositions = point1 - planePoints.point1
        let t = normal.dot(vector: diffPositions) / denominator
        let linePoint = planePoints.point1 + (lineDirection * t)
        let lineEquation = LineEquation(point: linePoint, vector: lineVector)
        return lineEquation
    }
}
