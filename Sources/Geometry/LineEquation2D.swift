import CoreFoundation

public struct LineEquation2D {
    public let a: CGFloat
    public let b: CGFloat
    public let c: CGFloat
    
    init(a: CGFloat, b: CGFloat, c: CGFloat) {
        self.a = a
        self.b = b
        self.c = c
    }
}

public extension LineEquation2D {
    
    static func create(p1: CGPoint, p2: CGPoint) -> LineEquation2D {
        let a = p1.y - p2.y
        let b = p2.x - p1.x
        let c = (p1.x - p2.x) * p1.y + (p2.y - p1.y) * p1.x
        return LineEquation2D(a: a, b: b, c: c)
    }
    
    func getNormalDistanceLength(from point: CGPoint) -> CGFloat {
        let numerator = abs(a * point.x + b * point.y + c)
        let denominator = sqrt(pow(a, 2) + pow(b, 2))
        let length = numerator / denominator
        return length
    }
}
