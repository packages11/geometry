import SceneKit

public struct LineOrientation {
    public let length: CGFloat
    public let transform: SCNMatrix4

    public init(length: CGFloat, transform: SCNMatrix4) {
        self.length = length
        self.transform = transform
    }
}

public extension LineOrientation {

    static func calculate(startPoint: SCNVector3, endPoint: SCNVector3) -> LineOrientation {
        let lookAt = endPoint - startPoint
        let length = lookAt.length
        let startTransform = SCNMatrix4.getTransform(startPoint: startPoint, endPoint: endPoint)
        let transform = SCNMatrix4MakeTranslation(0.0, length / 2.0, 0.0) * startTransform
        let lineOrientation = LineOrientation(length: CGFloat(length), transform: transform)
        return lineOrientation
    }

    static func calculate(with distance: Distance) -> LineOrientation {
        calculate(startPoint: distance.point1, endPoint: distance.point2)
    }
}
